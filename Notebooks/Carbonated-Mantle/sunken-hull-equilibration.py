# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.3.3
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# # Stixrude-Lithgow-Bertelloni min assemblage
# Required Python packages/modules

# +
import numpy as np
from os import path
import pandas as pd
import scipy.optimize as opt
from scipy import optimize
import scipy.linalg as lin
import scipy as sp
import sys
import sympy as sym

from collections import OrderedDict as odict

import matplotlib.pyplot as plt

import sunkenhull as hull
# -



# Required ENKI modules (ignore the error message from Rubicon running under Python 3.6+)

from thermoengine import coder, core, phases, model, equilibrate





def get_subsolidus_phases(database='Berman'):
    remove_phases = ['Liq','H2O']

    modelDB = model.Database(database)
    if database=='Stixrude':
        pure_soln_endmems = [
            'An', 'Ab', 'Spl', 'Hc', 'Fo', 'Fa', 'MgWds', 'FeWds', 'MgRwd',
            'FeRwd', 'En', 'Fs', 'MgTs', 'oDi', 'Di', 'Hd', 'cEn',
            'CaTs', 'Jd', 'hpcEn', 'hpcFs',  'MgAki', 'FeAki', 'AlAki', 'Prp',
            'Alm', 'Grs', 'Maj', 'NaMaj', 'MgPrv', 'FePrv',
            'AlPrv', 'MgPpv', 'FePpv', 'AlPpv', 'Per', 'Wus', 'MgCf', 'FeCf',
            'NaCf']

        # soln_keys_Stixrude = ['Fsp', 'Ol', 'Wds', 'Rwd', 'PrvS', 'PpvS', 'Opx',
        #                   'Cpx', 'hpCpx', 'AkiS', 'Grt', 'Fp', 'CfS', 'SplS']
        # pure_keys_Stixrude = ['CaPrv','Qz', 'Coe', 'Sti', 'Seif', 'Ky', 'Nph']
    elif database=='Berman':
        pure_soln_endmems = []

    else:
        assert False, [
            'Need to define list of pure solution endmembers to be removed '+
            'from the system, to avoid double counting.']


    phases = modelDB.phases
    [phases.pop(phs) for phs in remove_phases]
    [phases.pop(phs) for phs in pure_soln_endmems]

    return phases





# ### T,P, parameters and options for pseudo-phase generation

T = 1100.0                  # K
P = 300000.0                 # bars
P = 1.0                 # bars

# database='Berman'
database='Stixrude'



# +
phases = get_subsolidus_phases(database=database)
phs_sym, endmem_ids, mu_endmem, comp_endmem, sys_elems, phase_comps = (
    hull.system_energy_landscape(T, P, phases, prune_polymorphs=False))
# display(phs_sym, endmem_ids, mu, elem_comps, sys_elems)
Nelems = len(sys_elems)
Npts = mu_endmem.size

# Determine how many phases are pure
Npure = np.where(endmem_ids[::-1]>0)[0][0]
ind_soln = np.arange(mu_endmem.size-Npure)
ind_pure = mu_endmem.size-Npure + np.arange(Npure)
# -





display(sys_elems)
display(mu_endmem)


# # Define bulk composition

wt = np.random.rand(comp_endmem.shape[0])
wt = wt/np.sum(wt)
bulk_comp = np.dot(wt, comp_endmem)
# bulk_comp = np.array([0.59760393, 0.01614512, 0.04809849, 0.09232406, 0.1571301 ,
#        0.03304928, 0.05565109])

bulk_comp

T, P = 1100.0, 1.0
bulk_comp = np.array([0.59335532, 0.00968201, 0.09360127, 0.07404719, 0.15452804,
                      0.01331886, 0.06146731])





# # Intitalize endmember assemblage

# +
# wt_bulk, mu_bulk, ind_assem = hull.min_energy_linear_assemblage(bulk_comp, comp_endmem, mu_endmem, TOLmu=10, TOL=1e-5)
chempot, mu_bulk, wt_bulk, ind_assem = hull.min_energy_linear_assemblage(
    bulk_comp, comp_endmem, mu_endmem, TOLmu=10, TOL=1e-5, ignore_oxy=True)

print(ind_assem)
print(phs_sym[ind_assem])
print(endmem_ids[ind_assem])
print(chempot)

# -






dmu = mu_endmem-np.dot(chempot, comp_endmem.T)
print('dmu(assem) = ', dmu[ind_assem])
print('dmu(non-assem) = ', dmu[wt_bulk<1e-6])



# ## Update Affinities

# +
comp_soln, X_soln, Aff_soln, Aff_endmem, phase_names_soln, extras = (
    hull.update_phase_affinities(chempot, T, P, comp_endmem, phases, phase_comps))

Nsoln = Aff_soln.size-Npure
Aff_thresh = 1e3
# -

display(Aff_soln)
display(phase_names_soln[Aff_soln < 1*Aff_thresh])



# +
endmem_offset = 1000

Aff_compound = np.hstack((Aff_soln[:-Npure], Aff_endmem[ind_soln]+endmem_offset, Aff_endmem[ind_pure]))

phs_sym_compound = np.hstack((phase_names_soln[:-Npure], phs_sym[ind_soln], phs_sym[ind_pure]))

endmem_ids_compound = np.hstack((np.tile(-1,Nsoln), endmem_ids[ind_soln], endmem_ids[ind_pure]))

comp_compound = np.vstack((comp_soln[:-Npure,:], comp_endmem))

comp_compound_fit = hull.reduce_rank_comp(comp_compound)


# -



TOLmu = 10
dchempot, dmu_bulk_compound, wt_bulk_compound, ind_assem_compound = hull.min_energy_linear_assemblage(
    bulk_comp, comp_compound, Aff_compound, TOLmu=TOLmu, TOL=1e-5)
print('dmu_bulk = ', dmu_bulk_compound)
print('dchempot = ', dchempot)
display(phs_sym_compound[ind_assem_compound])
display(endmem_ids_compound[ind_assem_compound])

chempot += dchempot











# +
assem_phases = np.unique(phs_sym_compound[wt_bulk_compound>0])
for iphs_sym in assem_phases:
    imask_phs = phs_sym_compound==iphs_sym
    icomp_phs = comp_compound[imask_phs]
    iwt = wt_bulk_compound[imask_phs]
    iphs = phases[iphs_sym]
    
    iphs_endmem_comps = phase_comps[iphs_sym]
    
    iphs_comp = np.dot(iwt, icomp_phs)
    ioutput = np.linalg.lstsq(iphs_endmem_comps.T, iphs_comp)
    iendmem_mols = ioutput[0]
    
    print(iphs_comp)
    print(iendmem_mols)
    print(iphs_endmem_comps)
    print(iphs)
    print(iphs.endmember_names)
    
    ichempot = iphs.chem_potential(T, P, mol=iendmem_mols).squeeze()
    print(ichempot)
    
    print('---')


# -

phases

wt_bulk_compound

X_soln



# +

mu_compound_assem = Aff_compound[ind_assem_compound]
comp_compound_assem_cats = comp_compound_cats[ind_assem_compound,:]
output = np.linalg.lstsq(comp_compound_assem_cats, mu_compound_assem, rcond=-1)
dchempot = output[0]

chempot = np.hstack((0,dchempot)) + chempot_prev
print(dchempot)
print(chempot)
# -





def validate_linear_assem(T, P, database='Stixrude'):
    phases = get_subsolidus_phases(database=database)
    phs_sym, endmem_ids, mu_endmem, comp_endmem, sys_elems, phase_comps = (
        hull.system_energy_landscape(T, P, phases, prune_polymorphs=False))
    # display(phs_sym, endmem_ids, mu, elem_comps, sys_elems)
    Nelems = len(sys_elems)
    Npts = mu_endmem.size

    # # Define bulk composition

    wt = np.random.rand(comp_endmem.shape[0])
    wt = wt/np.sum(wt)
    bulk_comp = np.dot(wt, comp_endmem)

    bulk_comp_cats = bulk_comp[1:]
    comp_endmem_cats = comp_endmem[:,1:]
    print(bulk_comp_cats)

    # wt_bulk, mu_bulk, ind_assem = hull.min_energy_linear_assemblage(bulk_comp, comp_endmem, mu_endmem, TOLmu=10, TOL=1e-5)
    wt_bulk, mu_bulk, ind_assem = hull.min_energy_linear_assemblage(
        bulk_comp_cats, comp_endmem_cats, mu_endmem, TOLmu=10, TOL=1e-5)

    print(ind_assem)
    print(phs_sym[ind_assem])
    print(endmem_ids[ind_assem])

    mu_assem = mu_endmem[ind_assem]
    comp_assem = comp_endmem[ind_assem,:]
    output = np.linalg.lstsq(comp_assem, mu_assem, rcond=-1)
    chempot = output[0]

    dmu = mu_endmem-np.dot(chempot, comp_endmem.T)
    print('dmu(assem) = ', dmu[ind_assem])
    print('dmu(non-assem) = ', dmu[wt_bulk<1e-6])



T = 400.0                  # K
P = 10*(1e6/100)          # bars
validate_linear_assem(T, P, database='Stixrude')







# +
# phase_stable, nmol0_stable, comp_endmem_stable, extras = get_init_assem(bulk_comp, comp_endmem, mu_endmem)
