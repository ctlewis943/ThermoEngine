# Copyright (c) ENKI Development Team.
# Distributed under the terms of the Affero General Public License.
ARG BASE_CONTAINER=jupyter/all-spark-notebook
FROM $BASE_CONTAINER

LABEL maintainer="Mark Ghiorso <ghiorso@ofm-research.org>"

USER root

# compilers required for ENKI libgnustep-base-dev --no-install-recommends
RUN apt-get -y update && \
    apt-get -y upgrade && \
    apt-get -y dist-upgrade && \
    apt-get -y autoremove && \
    apt-get install -y  \
    gfortran \
    gcc \
    cmake \
    clang-10 \
    libgsl-dev \
    liblapack-dev \
    zip && \
    rm -rf /var/lib/apt/lists/*

USER $NB_UID

# install latex packages
RUN pip install --no-cache-dir jupyterlab_latex
RUN jupyter serverextension enable --sys-prefix jupyterlab_latex

# R packages including IRKernel which gets installed globally.
# Temporarily removed
# jupyter labextension install @jupyterlab/latex --no-build && \
# from install below (was first in list)
RUN conda install --quiet --yes \
	'openpyxl' \
    'snakeviz=2.0*' && \
    conda clean --all -f -y && \
    jupyter labextension install @enki-portal/jupyterlab-gitlab --no-build && \
    jupyter labextension install @enki-portal/shared --no-build && \
    jupyter labextension install @enki-portal/enkiintro --no-build && \
    jupyter lab build && \
    jupyter lab clean && \
    npm cache clean --force && \
    rm -rf /home/$NB_USER/.cache/yarn && \
    rm -rf /home/$NB_USER/.node-gyp && \
    fix-permissions $CONDA_DIR && \
    fix-permissions /home/$NB_USER

# install additional package...
RUN pip install --no-cache-dir nbgitpuller
RUN pip install --no-cache-dir deprecation
RUN pip install --no-cache-dir numdifftools
RUN pip install --no-cache-dir jupyterlab-gitlab-msg
RUN jupyter serverextension enable --sys-prefix jupyterlab_gitlab

USER root

ENV LD_LIBRARY /usr/local/lib

# Upgrade for 18.04 (bionic) does not establish symlinks for clang
#RUN ln -s /usr/bin/clang-8 /usr/bin/clang
#RUN ln -s /usr/bin/clang++-8 /usr/bin/clang++
#RUN ln -s /usr/bin/clang-cpp-8 /usr/bin/clang-cpp

RUN git clone https://gitlab.com/ENKI-portal/rubiconobjc.git && \
    cd ./rubiconobjc && \
    pip install --upgrade . && \
    cd .. && \
    rm -rf ./rubiconobjc

# Install GNUstep components from sources (requires Python 2.7, hence the path change)
ARG SAVE_PATH=$PATH
ENV PATH /usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

RUN git clone https://github.com/plaurent/gnustep-build && \
    cd gnustep-build/ubuntu-20.04-clang-10.0-runtime-2.0/ && \
    bash GNUstep-buildon-ubuntu2004.sh && \
    cd ../.. && \
    rm -rf ./gnustep-build

ENV PATH $SAVE_PATH

# Install ThermoEngine repository (note && \ missing from first line)
ENV RUNTIME_VERSION gnustep-2.0
RUN git clone http://gitlab.com/enki-portal/thermoengine.git && \
    cd ./thermoengine/Cluster && \
    make && \
    /usr/bin/install -c -p  ./obj/libswimdew.so.0.0.1 /usr/local/lib && \
    ln -s /usr/local/lib/libswimdew.so.0.0.1 /usr/local/lib/libswimdew.so.0 && \
    ln -s /usr/local/lib/libswimdew.so.0 /usr/local/lib/libswimdew.so && \
    /usr/bin/install -c -p  ./obj/libphaseobjc.so.0.0.1 /usr/local/lib && \
    ln -s /usr/local/lib/libphaseobjc.so.0.0.1 /usr/local/lib/libphaseobjc.so.0 && \
    ln -s /usr/local/lib/libphaseobjc.so.0 /usr/local/lib/libphaseobjc.so && \
    cd .. && \
    make pyinstall && \
    cd .. && \
    rm -rf ./thermoengine

USER $NB_UID
    