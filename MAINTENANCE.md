# Maintenance Notes
This file provides maintenance notes for the repository. The content is useful for owners and maintainers of the repository and supported services. It is not directly relevant to code contributors or general users.

This document contains sections on
- [Associated repositories](#associated-repositories)
- [Associated accounts](#associated-accounts)
- [Repository structure](#respository-structure)
- [Continuous Integration and Deployment (CI/CD)](#continuous-integration-and-deployment-cicd)
- [Settings](#settings)

## Associated repositories
- ENKI-portal/**ThermoEngine**
  - Main code repository (this repository)
- ENKI-portal/**jupyterlab-gitlab**
  - Jupyter Lab extension that provides access to the ENKI-portal group public repositories
- ENKI-portal/**jupyterlab_enkiintro**
  - Jupyter Lab extension that provides a splash screen on login to the ENKI cluster server
- ENKI-portal/**jupyterlab_shared**
  - Jupyter Lab extension that provides a tab for access to ThermoEngine notebooks
- ENKI-portal/**jupyterhub_custom**
  - Jupyter Hub template extensions that provide a login screen for the ENKI cluster server
- ENKI-portal/**ENKI-cluster-config**
  - Google cloud cluster configuration and deployment, automatically triggered by tagged uploads to ThermoEngine 
- ENKI-portal/**Theoretical_Geochemistry**
  - Textbook in Theoretical Geochemistry authored by H.C. Helgeson 
- ENKI-portal/**SulfLiq**
  - Python package that implements the thermodynamic model of Victor Kress for liquids in the system O-S-Fe-Ni-Cu 

## Associated accounts
Accounts on the following platforms are required:
- Google cloud
  - This account must be created at https://console.cloud.google.com/ and associated with a mechanism for billing and a project name for cluster creation. It is used by the shell scripts in **ThermoEngine**/Cluster and by the CI scripts in **ENKI-cluster-config** for interaction of GitLab with a Google Cloud Kubernetes cluster.
- npm
  - This account is required to deploy GitLab extensions that are uploaded to https://www.npmjs.com.  An access toten is generated from this account on the website and that toekn is defined as an environment variable in **Settings->CI/CD/Variables** on GitLab.com for the repository that performs an upload during the deploy phase of the CI/CD.  The token name is
    - NPM_TOKEN
    - It should be available to all environments and masked from display in the pipeline scripts.
    - Used by **jupyterlab-gitlab**, **jupyterlab_enkiintro**, and **jupyterlab_shared**
- PyPi
  - This account is required to deploy Python packages to PyPi (https://pypi.org) using twine. It is used by the CI/CD scripts to deloy Python packages.  Two tokens should be defined as environment variables in the repository at **Settings->CI/CD/Variable**.  The token names are
    - TWINE_PASSWORD and TWINE_USERNAME
    - These are generated at the PyPi website by creating a token for login, The username is __token__ and the password is the generated token value.
    - Used by **jupyterlab-gitlab**
- coveralls
  - This account is required to perform a code coverage analysis of a repository. The account is created on https://coveralls.io, and the repository is linked at that site.  Once designated, links are generated that describe converage.
  - Use your Gitlab account to login/register
  - Once a repository is selected for coverage at the coveralls site, a token will be generated. This token is used to upload coverage data from the Gitlab reposoitory using CI/CD. The token value should be set as a masked CI/CD environment variable in the relevant repository on Gitlab (Settings -> CI/CD -> Variables) using the name:
    - COVERALLS_REPO_TOKEN
  - Used by **ThermoEngine**
- zenodo
  - A zenodo account (https://zenodo.org) is required to generate DOIs that can be used to tag software in a repository
  - Used by **ThermoEngine** and **Theoretical_Geochemistry**
- ReadTheDocs
  - A ReadTheDocs account is requitred (https://readthedocs.org) for automated builds of documentation 
  - Used by **ThermoEngine**

## Respository structure
- two branches, master and Documentation, both protected
- README file 
- GNU AGPLv3 public license; sorce must be included and exposed in any subsequent use of the code, otherwise, code is open source
- CHANGELOG documents every major push to master
- CONTRIBUTING document describes workflow for contributing to the code base
- Issues
  - Service Desk is turned on so that issues may be emailed to the repository
  - Issue templates are stored in .gitlab/issue_templates
- Packages -> Container Registry
  - This is the Gitlab container registry that holds docker images for kubernetes cluster or standalone deployment
- Wiki
  - This is a wiki for the repository. 

## Continuous Integration and Deployment (CI/CD)
- CI/CD integration is scripted in the *.gitlab-ci.yml* file. There are four stages:
  - build (executed on push to master)
    - create docker container image on upload to the master branch; uses caching; pushes to GitLab container registry associated with the repository
  - test (executed on push to master)
    - executes tests on the container image using pytest called from a shell script called test-image.sh
    - test results are used to assess code coverage
  - release (executaed on a tagged push to master)
    - copies the image generated under the build stage to a version tagged latest, and pushes that image to the container repository.
  - deploy
    - pages (executed on push to Documentation branch)
      - copies sphinx generated documentation into the Pages website, which is exposed at https://enki-portal.gitlab.io/
  - Triggering
    - When a tagged commit is pushed to the master branch, in addition to the events described under release, the image is deployed by the **ENKI-cluster-config** project using CI/CD to an associated Google Cloud Kubernetes cluster.

## Settings
- General
    - Visibility, project features, permissions
      - Public
    - Badges
      - Set for status of ReadTheDocs upload (see below)
      - Pipeline status
      - Code coverage
      - Zenodo DOI
    - Service Desk
      - Set to on
      - Note email address for issues
- Members
  - Special members
    - Owners: Wolf and Ghiorso
    - Maintainers: Johnson (for Documentation branch access)
  - ENKI-portal group
    - These members: Ghiorso, Wolf, Johnson
- Webhooks
  - A ReadTheDocs hook is defined so that on pushing the Documentation branch, ReadTheDocs generates sphinx-based documentation
- Repository
  - Protected branches
    - Documentation is set up as a protected branch (master is by default). Only maintainers or owners can merge into or push a protected branch
- CI/CD
  - Variables
    - Environment variables are set here for CI/CD builds
- Pages
  - Pages are served (using SSL certificates). These html pages serve a copy of locally generated documentation, which mirrors documentation automaticlly generated via a web hook to ReadThe Docs.
